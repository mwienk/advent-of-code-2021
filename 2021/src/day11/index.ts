import run from "aocrunner";

const parseInput = (rawInput: string) =>
  rawInput.split("\n").map((row) => row.split("").map((str) => Number(str)));

type Coord = {
  x: number;
  y: number;
};

const getAdjacent = ({ x, y }: Coord): Coord[] => {
  return [
    { x: x - 1, y: y - 1 },
    { x, y: y - 1 },
    { x: x + 1, y: y - 1 },
    { x: x - 1, y },
    { x: x + 1, y },
    { x: x - 1, y: y + 1 },
    { x, y: y + 1 },
    { x: x + 1, y: y + 1 },
  ].filter(({ x, y }) => x >= 0 && y >= 0 && y <= 9 && x <= 9);
};

const increment = (grid: number[][]): Coord[] => {
  const ready = [];
  // Increase all numbers by 1
  for (let x = 9; x >= 0; x--) {
    for (let y = 9; y >= 0; y--) {
      if (grid[y][x] === 9) {
        // Store nines
        ready.push({ x, y });
      }
      grid[y][x]++;
    }
  }
  return ready;
};

const handleFlashes = (grid, ready: Coord[]) => {
  // Handle nines
  for (let i = 0; i < ready.length; i++) {
    getAdjacent(ready[i]).forEach(({ x, y }) => {
      if (grid[y][x] === 9) {
        ready.push({ x, y });
      }
      grid[y][x]++;
    });
  }

  // Reset nines
  ready.forEach(({ x, y }) => (grid[y][x] = 0));
};

const progress = (grid: number[][]): number => {
  const readyToFlash = increment(grid);
  handleFlashes(grid, readyToFlash);
  return readyToFlash.length;
};

const print = (grid: number[][]) =>
  console.log(grid.map((row) => row.join("")).join("\n"));

const part1 = (rawInput: string) => {
  const grid = parseInput(rawInput);
  let flashes = 0;
  let i = 100;
  while (i > 0) {
    flashes += progress(grid);
    i--;
  }
  return flashes;
};

const part2 = (rawInput: string) => {
  const grid = parseInput(rawInput);
  let i = 0;
  while (true) {
    const flashes = progress(grid);
    i++;
    if (flashes === 100) {
      return i;
    }
  }
};

run({
  part1: {
    tests: [
      {
        input: `
          5483143223
          2745854711
          5264556173
          6141336146
          6357385478
          4167524645
          2176841721
          6882881134
          4846848554
          5283751526
        `,
        expected: 1656,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          5483143223
          2745854711
          5264556173
          6141336146
          6357385478
          4167524645
          2176841721
          6882881134
          4846848554
          5283751526
        `,
        expected: 195,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

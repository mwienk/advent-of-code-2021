import run from "aocrunner";

const addSortedBinary = (arr: Node[], node: Node, lower, upper) => {
  if (upper - lower === 1) {
    // binary search ends, we need to insert the element around here
    if (node < arr[lower]) arr.splice(lower, 0, node);
    else if (node > arr[upper]) arr.splice(upper + 1, 0, node);
    else arr.splice(upper, 0, node);
  } else {
    // we look for the middle point
    const midPoint = Math.floor((upper - lower) / 2) + lower;
    // depending on the value in the middle, we repeat the operation only on one slice of the array, halving it each time
    node < arr[midPoint]
      ? addSortedBinary(arr, node, lower, midPoint)
      : addSortedBinary(arr, node, midPoint, upper);
  }
};

const addSorted = (arr, node: Node) =>
  addSortedBinary(arr, node, 0, arr.length - 1 > 1 ? arr.length : 1);

type Coord = {
  x: number;
  y: number;
};

class Node {
  public travelCost = Infinity;
  public visited = false;
  public parent;
  constructor(public readonly cost: number, public readonly coord: Coord) {}

  valueOf() {
    return this.travelCost;
  }
}

const getNeigbors = ({ x, y }: Coord, max: number): Coord[] =>
  [
    { x: x + 1, y },
    { x: x - 1, y },
    { x, y: y - 1 },
    { x, y: y + 1 },
  ].filter(({ x, y }) => x >= 0 && x < max && y >= 0 && y < max);

const parseInput = (rawInput: string) =>
  rawInput
    .split("\n")
    .map((line, y) =>
      line.split("").map((value, x) => new Node(Number(value), { x, y })),
    );

const iterate = (grid: Node[][], unvisited: Node[]) => {
  const current = unvisited.shift();
  const neighbors = getNeigbors(current.coord, grid.length);
  for (let { x, y } of neighbors) {
    const node = grid[y][x];
    if (node.visited) {
      continue;
    }
    const travelCost = current.travelCost + node.cost;
    if (travelCost < node.travelCost) {
      // Remove first
      node.travelCost = travelCost;
      node.parent = current;
      if (!unvisited.includes(node)) {
        addSorted(unvisited, node);
      }
    }
  }
  current.visited = true;
};

const multiply = (grid: Node[][]) => {
  const initialLength = grid.length;
  // Copy horizontally
  for (let ix = 0; ix < 5; ix++) {
    // And vertically
    for (let iy = 0; iy < 5; iy++) {
      // All y values
      for (let y = 0; y < initialLength; y++) {
        // And x values
        for (let x = 0; x < initialLength; x++) {
          const newY = y + initialLength * iy;
          const newX = x + initialLength * ix;
          grid[newY] = grid[newY] || [];
          const cost = (grid[y][x].cost + ix + iy) % 9;
          grid[newY][newX] = new Node(cost === 0 ? 9 : cost, {
            x: newX,
            y: newY,
          });
        }
      }
    }
  }
};

const part1 = (rawInput: string) => {
  const grid = parseInput(rawInput);
  // Set initial cost to zero
  grid[0][0].travelCost = 0;
  const unvisited = [grid[0][0]];

  // Calculate all costs
  while (unvisited.length) {
    iterate(grid, unvisited);
  }

  // Return the least cost to the last element
  return grid[grid.length - 1][grid.length - 1].travelCost;
};

const part2 = (rawInput: string) => {
  const grid = parseInput(rawInput);
  multiply(grid);

  // Set initial cost to zero
  grid[0][0].travelCost = 0;
  const unvisited = [grid[0][0]];

  // Calculate all costs
  while (unvisited.length) {
    iterate(grid, unvisited);
  }

  // Return the least cost to the last element
  return grid[grid.length - 1][grid.length - 1].travelCost;
};

run({
  part1: {
    tests: [
      {
        input: `
          1163751742
          1381373672
          2136511328
          3694931569
          7463417111
          1319128137
          1359912421
          3125421639
          1293138521
          2311944581
        `,
        expected: 40,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          1163751742
          1381373672
          2136511328
          3694931569
          7463417111
          1319128137
          1359912421
          3125421639
          1293138521
          2311944581
        `,
        expected: 315,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

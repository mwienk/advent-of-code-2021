import run from "aocrunner";

const OPENERS = "([{<";
const CLOSERS = ")]}>";
const POINTS = { "}": 1197, "]": 57, ">": 25137, ")": 3 };

const parseInput = (rawInput: string) => {
  return rawInput.split("\n");
};

const isNotMatching = (lastOpener, token) =>
  CLOSERS.indexOf(token) !== -1 &&
  OPENERS.indexOf(lastOpener) !== CLOSERS.indexOf(token);

const handleLine = (line) => {
  let openers = "";
  let lastOpener = "";
  for (let token of line) {
    if (isNotMatching(lastOpener, token)) {
      return { score: POINTS[token], openers };
    }
    if (OPENERS.includes(token)) {
      openers += token;
      lastOpener = token;
    } else {
      openers = openers.slice(0, -1);
      lastOpener = openers[openers.length - 1];
    }
  }
  return { score: 0, openers };
};

const getAutocompleteScore = (line) => {
  return line.openers
    .split("")
    .reverse()
    .reduce((acc, opener) => acc * 5 + OPENERS.indexOf(opener) + 1, 0);
};

const part1 = (rawInput: string) => {
  return parseInput(rawInput)
    .map(handleLine)
    .reduce((acc, line) => acc + line.score, 0);
};

const part2 = (rawInput: string) => {
  const autocompleteScores = parseInput(rawInput)
    .map(handleLine)
    .filter((total) => total.score === 0)
    .map(getAutocompleteScore)
    .sort((a, b) => a - b);
  return autocompleteScores[Math.floor(autocompleteScores.length / 2)];
};

run({
  part1: {
    tests: [
      {
        input: `
          [({(<(())[]>[[{[]{<()<>>
          [(()[<>])]({[<{<<[]>>(
          {([(<{}[<>[]}>{[]{[(<()>
          (((({<>}<{<{<>}{[]{[]{}
          [[<[([]))<([[{}[[()]]]
          [{[{({}]{}}([{[{{{}}([]
          {<[[]]>}<{[{[{[]{()[[[]
          [<(<(<(<{}))><([]([]()
          <{([([[(<>()){}]>(<<{{
          <{([{{}}[<[[[<>{}]]]>[]]
        `,
        expected: 26397,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          [({(<(())[]>[[{[]{<()<>>
          [(()[<>])]({[<{<<[]>>(
          {([(<{}[<>[]}>{[]{[(<()>
          (((({<>}<{<{<>}{[]{[]{}
          [[<[([]))<([[{}[[()]]]
          [{[{({}]{}}([{[{{{}}([]
          {<[[]]>}<{[{[{[]{()[[[]
          [<(<(<(<{}))><([]([]()
          <{([([[(<>()){}]>(<<{{
          <{([{{}}[<[[[<>{}]]]>[]]
        `,
        expected: 288957,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

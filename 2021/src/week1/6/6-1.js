const fs = require("fs");

function extract(filename) {
  const inputs = fs
    .readFileSync(filename, {encoding: 'utf8'})
    .trim()
    .split(',');

  return {
    inputs,
  }
}

function transform(inputs) {
  return inputs.map((days) => parseInt(days));
}

function progressDay(lanternfish) {
  return lanternfish.flatMap((fish) => fish === 0 ? [6, 8] : [fish-1]);
}

function main(days) {
  const { inputs } = extract('./input');
  const lanternfish = transform(inputs);

  const result = [...Array(days).keys()].reduce(progressDay, lanternfish);
  console.log(result.length);
}

main(80);

module.exports = {
  extract,
};

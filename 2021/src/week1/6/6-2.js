const { extract } = require("./6-1");

function transform(inputs) {
  const counts = new Array(9).fill(0);
  inputs.forEach((input) => counts[parseInt(input)]++);
  return counts;
}

function progressDay(lanternfish) {
  const spawners = lanternfish[0];
  lanternfish[0] = lanternfish[1];
  lanternfish[1] = lanternfish[2];
  lanternfish[2] = lanternfish[3];
  lanternfish[3] = lanternfish[4];
  lanternfish[4] = lanternfish[5];
  lanternfish[5] = lanternfish[6];
  lanternfish[6] = lanternfish[7] + spawners;
  lanternfish[7] = lanternfish[8];
  lanternfish[8] = spawners;
  return lanternfish;
}

function main(days) {
  const { inputs } = extract('./input');
  const initialCounts = transform(inputs);

  const result = [...Array(days).keys()]
    .reduce(progressDay, initialCounts)
    .reduce((sum, count) => sum + count);
  console.log(result);
}

main(256);

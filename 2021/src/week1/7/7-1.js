const fs = require("fs");

function extract(filename) {
  const inputs = fs
    .readFileSync(filename, {encoding: 'utf8'})
    .trim()
    .split(',');

  return {
    inputs,
  }
}

function transform(inputs) {
  return inputs.map((pos) => parseInt(pos));
}

function median(numbers) {
  const sorted = numbers.sort((a, b) => a - b);
  const middle = Math.floor(sorted.length / 2);

  if (sorted.length % 2 === 0) {
    return (sorted[middle - 1] + sorted[middle]) / 2;
  }

  return sorted[middle];
}

function main() {
  const { inputs } = extract('./input');
  const positions = transform(inputs);

  const medianValue = median(positions);
  const totalDeviation = positions.reduce((sum, pos) => sum + Math.abs(medianValue - pos), 0);
  console.log(totalDeviation);
}

main();

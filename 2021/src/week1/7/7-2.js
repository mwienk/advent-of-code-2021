const fs = require("fs");

function extract(filename) {
  const inputs = fs
    .readFileSync(filename, {encoding: 'utf8'})
    .trim()
    .split(',');

  return {
    inputs,
  }
}

function transform(inputs) {
  return inputs.map((pos) => parseInt(pos));
}

function main() {
  const { inputs } = extract('./input');
  const positions = transform(inputs);
  const total = positions.reduce((sum, pos) => sum + pos, 0);
  const averages = [
    Math.floor(total / positions.length),
    Math.ceil(total / positions.length),
  ];
  const totalDeviation = positions.reduce((result, pos) => {
    const n0 = Math.abs(averages[0] - pos);
    const total0 = (n0 * (n0 + 1) / 2);
    const n1 = Math.abs(averages[1] - pos);
    const total1 = (n1 * (n1 + 1) / 2);
    return [result[0] + total0, result[1] + total1];
  }, [0, 0]);

  console.log(Math.min(...totalDeviation));
}

main();

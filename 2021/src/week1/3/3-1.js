const fs = require("fs");

const inputs = fs
  .readFileSync('./input', { encoding: 'utf8' })
  .split('\n')
  .filter(item => item)
  .map((input) => parseInt(input, 2));

// Check if bitmask matches bitwise and on zeroes with 1 on position
const findMostCommonDigit = (numbers, position) => numbers.filter(i => (i & 1 << position) === 1 << position).length > (numbers.length / 2) ? 1 : 0;

const result = [...Array(12).keys()]
  .reverse() // Binary works RTL
  .map((position) => findMostCommonDigit(inputs, position))
  .join('') // Turn into string

const gamma = parseInt(result,2);
const epsilon = ~ gamma & 0b111111111111; // NOT gamma with in 12 bit
console.log('GAMMA  :', gamma);
console.log('EPSILON:', epsilon);
console.log('TOTAL:', gamma * epsilon);

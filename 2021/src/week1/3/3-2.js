const fs = require("fs");
const length = 12;

const inputs = fs
  .readFileSync('./input', { encoding: 'utf8' })
  .split('\n')
  .filter(item => item)
  .map((input) => parseInt(input, 2));

// Check if bitmask matches bitwise and on zeroes with 1 on position
const findMostCommonDigit = (numbers, position) => numbers.filter(i => (i & 1 << position) === 1 << position).length >= (numbers.length / 2) ? 1 : 0;

const filterNumbers = (selector) => (numbers) => {
  return [...Array(length).keys()]
    .reverse()
    .reduce((remaining, position) => {
      if (remaining.length === 1) {
        return remaining;
      }
      const mostCommon = selector(remaining, position);
      return remaining.filter((input) => (input & 1 << position) === mostCommon << position);
    }, numbers)[0];
}


const oxygen = filterNumbers(findMostCommonDigit)(inputs);
const c02 = filterNumbers((remaining, position) => findMostCommonDigit(remaining, position) ? 0 : 1)(inputs);

console.log('OXYGEN: ', oxygen);
console.log('CO2:', c02);
console.log('TOTAL', c02 * oxygen)

const fs = require("fs");

const result = fs
  .readFileSync('./input', { encoding: 'utf8' })
  .split('\n')
  .filter(item => item)
  .map((cmd) => cmd.split(' '))
  .reduce((result, [cmd, arg]) => {
    switch (cmd) {
      case 'forward':
        return { ...result, horizontal: result.horizontal + parseInt(arg), depth: result.depth + result.aim * parseInt(arg)};
      case 'down':
        return { ...result, aim: result.aim + parseInt(arg) };
      case 'up':
        return { ...result, aim: result.aim - parseInt(arg) };
    }
  }, {
    aim: 0,
    depth: 0,
    horizontal: 0,
  });

console.log(result.depth * result.horizontal);

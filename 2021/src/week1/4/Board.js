const IDX = ['B', 'I', 'N', 'G', 'O'];

class Board {
  indexed = new Map();
  counts = null;
  bingo = false;

  constructor(rows) {
    this.fill(rows);
    this.counts = IDX.reduce((counts, col, idx) => ({...counts, [col]: 5, [idx]: 5}), {});
  }

  fill(rows) {
    rows.map((row, idy) => row
      .split(' ')
      .filter(i => i)
      .forEach((number, idx) => this.indexed.set(number, `${IDX[idx]}${idy}`))
    );
  }

  handleDrawn(number) {
    if (this.indexed.has(number)) {
      const index = this.indexed.get(number);
      this.counts[index[0]]--;
      this.counts[index[1]]--;
      if (this.counts[index[0]] === 0) {
        this.bingo = true;
        console.log(`Bingo on column ${index[0]}`);
      } else if (this.counts[index[1]] === 0) {
        this.bingo = true;
        console.log(`Bingo on row ${index[1]}`);
      }
      this.indexed.delete(number)
    }
  }

  hasBingo() {
    return this.bingo;
  }

  getScore(lastNumber) {
    let score = 0;
    for (let value of this.indexed.keys()) {
      score += parseInt(value);
    }
    return score * lastNumber;
  }
}

module.exports = Board;

const fs = require("fs");
const Board = require("./Board");

function extract() {
  const inputs = fs
    .readFileSync('./input', {encoding: 'utf8'})
    .split('\n\n')
    .map((item) => item.split('\n'));

  const [drawn, ...boardData] = inputs;
  return {
    drawn: drawn[0].split(','),
    boardData,
  }
}

function transform(boardData) {
  return boardData.map(rows => new Board(rows));
}

function handleDrawnPerBoard(boards, number) {
  for (const board of boards) {
    board.handleDrawn(number);
    if (board.hasBingo()) {
      return board.getScore(number);
    }
  }
}

function runDrawnNumbers(boards, numbers) {
  for (const number of numbers) {
    const winner = handleDrawnPerBoard(boards, number);
    if (winner) {
      return winner;
    }
  }
}

function main() {
  const { drawn, boardData } = extract();
  const boards = transform(boardData);

  const score = runDrawnNumbers(boards, drawn);
  console.log(score);
}

main();

module.exports = {
  transform,
  extract,
}

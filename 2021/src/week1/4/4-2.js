const { extract, transform } = require("./4-1");

function runDrawnNumbers(boards, numbers) {
  for (const number of numbers) {
    boards.forEach((board) => board.handleDrawn(number));
    if (boards.length === 1 && boards[0].hasBingo()) {
      return boards[0].getScore(number);
    }
    boards = boards.filter((board) => !board.hasBingo());
  }
}

function main() {
  const { drawn, boardData } = extract();
  const boards = transform(boardData);

  const score = runDrawnNumbers(boards, drawn);
  console.log(score);
}

main();

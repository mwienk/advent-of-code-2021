const fs = require("fs");

const result = fs
  .readFileSync('./input', { encoding: 'utf8' })
  .split('\n')
  .map((depth) => parseInt(depth))
  .filter((depth) => !isNaN(depth))
  .reduce((acc, _, index, arr) =>
    index + 3 > arr.length ? acc : [...acc, arr[index] + arr[index + 1] + arr[index + 2]],
    []
  )
  .reduce((result, depth) => ({
    total: result.prev !== null && depth > result.prev ? result.total + 1 : result.total,
    prev: depth
  }), {
    total: 0,
    prev: null,
  });

console.log(result.total);

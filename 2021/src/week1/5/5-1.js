const fs = require("fs");

function extract(filename) {
  const inputs = fs
    .readFileSync(filename, {encoding: 'utf8'})
    .trim()
    .split('\n');

  return {
    inputs,
  }
}

function transform(inputs) {
  return inputs
    .map((input) => input.split(' -> ')
      .map(coord => coord.split(',')
        .map(coord => parseInt(coord))
      )
    );
}

function getEmptyGrid(x, y) {
  return Array.from({ length: y }, () => (new Array(x).fill(0)));
}

function getMaxValues(vents) {
  return [
    vents.reduce((max, line) => Math.max(line[0][0], line[1][0], max), 0),
    vents.reduce((max, line) => Math.max(line[0][1], line[1][1], max), 0),
  ]
}

function draw(grid) {
  grid.forEach((yAxis) => {
    yAxis.forEach((xValue) => {
      process.stdout.write(`${xValue ? xValue : '.'}`);
    });
    process.stdout.write("\n");
  });
}

function countOverlap(grid) {
  return grid.reduce((total, yAxis) => total + yAxis.reduce((xTotal, xValue) => xTotal + (xValue > 1 ? 1 : 0), 0), 0);
}


function fillGrid(vents, emptyGrid) {
  return vents.reduce((counts, line) => {
    const x_start = Math.min(line[0][0], line[1][0]);
    const x_end = Math.max(line[0][0], line[1][0]);
    const y_start = Math.min(line[0][1], line[1][1]);
    const y_end = Math.max(line[0][1], line[1][1]);
    if (x_start === x_end) {
      for (let y = y_start; y <= y_end; y++) {
        counts[y][x_start]++;
      }
    } else if (y_start === y_end) {
      for (let x = x_start; x <= x_end; x++) {
        counts[y_start][x]++;
      }
    }

    return counts;
  }, emptyGrid);
}

function main() {
  const { inputs } = extract('./input');
  const vents = transform(inputs);
  const [x_max, y_max] = getMaxValues(vents);

  const grid = fillGrid(vents, getEmptyGrid(x_max + 1, y_max + 1))
  draw(grid);
  console.log(countOverlap(grid));
}

main();


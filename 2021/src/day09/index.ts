import run from "aocrunner";

type Coord = {
  x: number;
  y: number;
};

type CoordValue = Coord & {
  value: number;
};

const parseInput = (rawInput: string) =>
  rawInput.split("\n").map((row) => row.split("").map((str) => Number(str)));

const findLowPoints = (input: number[][]): CoordValue[] => {
  const yMax = input.length - 1;
  const xMax = input[0].length - 1;
  return input.flatMap((line, yIndex) =>
    line.flatMap((number, xIndex) =>
      (xIndex === 0 || number < line[xIndex - 1]) &&
      (xIndex === xMax || number < line[xIndex + 1]) &&
      (yIndex === 0 || number < input[yIndex - 1][xIndex]) &&
      (yIndex === yMax || number < input[yIndex + 1][xIndex])
        ? [{ x: xIndex, y: yIndex, value: number }]
        : [],
    ),
  );
};

const getNeigborsCalculator =
  (yMax: number, xMax: number) =>
  ({ x, y }: Coord): Coord[] => {
    const coords = [];
    if (x < xMax) {
      coords.push({ x: x + 1, y });
    }
    if (x > 0) {
      coords.push({ x: x - 1, y });
    }
    if (y < yMax) {
      coords.push({ x, y: y + 1 });
    }
    if (y > 0) {
      coords.push({ x, y: y - 1 });
    }
    return coords;
  };

const getBasinCalculator = (input: number[][]) => {
  const yMax = input.length - 1;
  const xMax = input[0].length - 1;
  const getNeighbors = getNeigborsCalculator(yMax, xMax);

  const counted = [];
  function calculate(coord: Coord) {
    counted.push(`${coord.x}.${coord.y}`);
    if (input[coord.y][coord.x] === 9) {
      return 0;
    }

    return getNeighbors(coord)
      .map((neighbor) =>
        counted.includes(`${neighbor.x}.${neighbor.y}`)
          ? 0
          : calculate(neighbor),
      )
      .reduce((a, b) => a + b, 1);
  }

  return calculate;
};

const part1 = (rawInput: string) => {
  const input = parseInput(rawInput);
  return findLowPoints(input).reduce(
    (total, point) => total + point.value + 1,
    0,
  );
};

const part2 = (rawInput: string) => {
  const input = parseInput(rawInput);
  const findBasinSize = getBasinCalculator(input);
  return findLowPoints(input)
    .map(findBasinSize)
    .sort((a, b) => a - b)
    .reverse()
    .slice(0, 3)
    .reduce((a, b) => a * b);
};

run({
  part1: {
    tests: [
      {
        input: `
          2199943210
          3987894921
          9856789892
          8767896789
          9899965678
        `,
        expected: 15,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          2199943210
          3987894921
          9856789892
          8767896789
          9899965678
        `,
        expected: 1134,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

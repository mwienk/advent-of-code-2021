import run from "aocrunner";

class Cave {
  name: string;
  reusable = false;
  connections: Cave[] = [];

  addConnection(tunnel: Cave, source = true) {
    this.connections.push(tunnel);
    if (source) {
      tunnel.addConnection(this, false);
    }
  }

  constructor(name) {
    this.name = name;
    this.reusable = /^[A-Z]*$/.test(name);
  }
}

const parseInput = (rawInput: string) => {
  const registry = new Map<string, Cave>();

  const connections = rawInput.split("\n");
  connections
    .flatMap((conn) => conn.split("-"))
    .forEach((name) => registry.set(name, new Cave(name)));

  connections.forEach((conn) => {
    const [source, target] = conn.split("-");
    registry.get(source).addConnection(registry.get(target));
  });
  return registry;
};

const traverse = (
  cave: Cave,
  usedTwice = true,
  visited: string[] = [],
): string[] => {
  if (cave.name === "end") {
    return [[...visited, "end"].join(",")];
  }
  if (!cave.reusable) {
    if (visited.includes(cave.name)) {
      if (usedTwice || cave.name === "start" || cave.name === "end") {
        return [];
      }
      usedTwice = true;
    }
  }
  return cave.connections
    .map((conn) => traverse(conn, usedTwice, [...visited, cave.name]))
    .flat();
};

const part1 = (rawInput: string) => {
  const registry = parseInput(rawInput);
  const paths = traverse(registry.get("start"));
  return paths.length;
};

const part2 = (rawInput: string) => {
  const registry = parseInput(rawInput);
  const paths = traverse(registry.get("start"), false);
  return paths.length;
};

run({
  part1: {
    tests: [
      {
        input: `
          start-A
          start-b
          A-c
          A-b
          b-d
          A-end
          b-end
        `,
        expected: 10,
      },
      {
        input: `
          dc-end
          HN-start
          start-kj
          dc-start
          dc-HN
          LN-dc
          HN-end
          kj-sa
          kj-HN
          kj-dc
        `,
        expected: 19,
      },
      {
        input: `
          fs-end
          he-DX
          fs-he
          start-DX
          pj-DX
          end-zg
          zg-sl
          zg-pj
          pj-he
          RW-he
          fs-DX
          pj-RW
          zg-RW
          start-pj
          he-WI
          zg-he
          pj-fs
          start-RW
        `,
        expected: 226,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          start-A
          start-b
          A-c
          A-b
          b-d
          A-end
          b-end
        `,
        expected: 36,
      },
      {
        input: `
          dc-end
          HN-start
          start-kj
          dc-start
          dc-HN
          LN-dc
          HN-end
          kj-sa
          kj-HN
          kj-dc
        `,
        expected: 103,
      },
      {
        input: `
          fs-end
          he-DX
          fs-he
          start-DX
          pj-DX
          end-zg
          zg-sl
          zg-pj
          pj-he
          RW-he
          fs-DX
          pj-RW
          zg-RW
          start-pj
          he-WI
          zg-he
          pj-fs
          start-RW
        `,
        expected: 3509,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

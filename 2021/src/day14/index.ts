import run from "aocrunner";

const parseInput = (rawInput: string) => {
  const [[input], rules] = rawInput
    .split("\n\n")
    .map((part) => part.split("\n"));

  const pairs: Record<string, number> = {};
  for (let i = 0; i < input.length - 1; i++) {
    pairs[`${input[i]}${input[i + 1]}`] =
      (pairs[`${input[i]}${input[i + 1]}`] ?? 0) + 1;
  }

  return {
    lastLetter: input[input.length - 1],
    input: pairs,
    rules: rules.reduce((ruleBook, rule) => {
      const [section, result] = rule.split(" -> ");
      ruleBook[section] = result;
      return ruleBook;
    }, {}),
  };
};

const getPolimerizer =
  (rules: Record<string, string>) => (input: Record<string, number>) => {
    const newpairs = {};
    for (const pair in input) {
      const insertion = rules[`${pair}`] ?? false;
      if (insertion) {
        newpairs[pair[0] + insertion] =
          (newpairs[pair[0] + insertion] ?? 0) + input[pair];
        newpairs[insertion + pair[1]] =
          (newpairs[insertion + pair[1]] ?? 0) + input[pair];
      }
    }
    return newpairs;
  };

const calculate = (input: Record<string, number>, lastLetter: string) => {
  const counts: Record<string, number> = {};
  for (const pair in input) {
    if (input[pair] === 0) {
      continue;
    }
    counts[pair[0]] = (counts[pair[0]] ?? 0) + input[pair];
  }
  counts[lastLetter]++;
  let min;
  let max;
  for (let char in counts) {
    if (!min || min > counts[char]) {
      min = counts[char];
    }
    if (!max || max < counts[char]) {
      max = counts[char];
    }
  }
  return max - min;
};

const part1 = (rawInput: string) => {
  const { input, rules, lastLetter } = parseInput(rawInput);
  let result = input;
  const polimerize = getPolimerizer(rules);

  for (let i = 0; i < 10; i++) {
    result = polimerize(result);
  }

  return calculate(result, lastLetter);
};

const part2 = (rawInput: string) => {
  const { input, rules, lastLetter } = parseInput(rawInput);
  let result = input;
  const polimerize = getPolimerizer(rules);

  for (let i = 0; i < 40; i++) {
    result = polimerize(result);
  }

  return calculate(result, lastLetter);
};

run({
  part1: {
    tests: [
      {
        input: `
          NNCB

          CH -> B
          HH -> N
          CB -> H
          NH -> C
          HB -> C
          HC -> B
          HN -> C
          NN -> C
          BH -> H
          NC -> B
          NB -> B
          BN -> B
          BB -> N
          BC -> B
          CC -> N
          CN -> C
        `,
        expected: 1588,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          NNCB

          CH -> B
          HH -> N
          CB -> H
          NH -> C
          HB -> C
          HC -> B
          HN -> C
          NN -> C
          BH -> H
          NC -> B
          NB -> B
          BN -> B
          BB -> N
          BC -> B
          CC -> N
          CN -> C
        `,
        expected: 2188189693529,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

import run from "aocrunner";

const SEGMENT_COUNTS = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6];

const onlyUnique = (signal) => {
  switch (signal.length) {
    case SEGMENT_COUNTS[1]:
    case SEGMENT_COUNTS[4]:
    case SEGMENT_COUNTS[7]:
    case SEGMENT_COUNTS[8]:
      return true;
    default:
      return false;
  }
};

const parseLine = (line: string) =>
  line.split(" | ").map((signals) => signals.split(" "));

const parseInput = (
  rawInput: string,
): { training: string[][]; output: string[][] } =>
  rawInput
    .split("\n")
    .map(parseLine)
    .reduce(
      (acc, line) => {
        acc.training.push(line[0]);
        acc.output.push(line[1]);
        return acc;
      },
      { training: [], output: [] },
    );

const part1 = (rawInput: string) => {
  const { output } = parseInput(rawInput);
  return output.flat().filter(onlyUnique).length;
};

const normalize = (value) => value.split("").sort().join();

const register = (registry: string[]) => (signal) => {
  switch (signal.length) {
    case SEGMENT_COUNTS[1]:
      registry[1] = normalize(signal);
      break;
    case SEGMENT_COUNTS[4]:
      registry[4] = normalize(signal);
      break;
    case SEGMENT_COUNTS[7]:
      registry[7] = normalize(signal);
      break;
    case SEGMENT_COUNTS[8]:
      registry[8] = normalize(signal);
      break;
    case 5: {
      if (signal.match(new RegExp(`[${registry[1]}]`, "g")).length === 2) {
        registry[3] = normalize(signal);
      } else if (
        signal.match(new RegExp(`[${registry[4]}]`, "g")).length === 3
      ) {
        registry[5] = normalize(signal);
      } else {
        registry[2] = normalize(signal);
      }
      break;
    }
    case 6: {
      if (signal.match(new RegExp(`[${registry[1]}]`, "g")).length !== 2) {
        registry[6] = normalize(signal);
      } else if (
        signal.match(new RegExp(`[${registry[4]}]`, "g")).length === 4
      ) {
        registry[9] = normalize(signal);
      } else {
        registry[0] = normalize(signal);
      }
    }
  }
};

const part2 = (rawInput: string) => {
  const { training, output } = parseInput(rawInput);
  return training.reduce((sum, signals, line) => {
    const registry = [];
    signals.filter(onlyUnique).forEach(register(registry));
    signals.forEach(register(registry));
    const lineOutput = output[line].reduce((outputStr, signal) => {
      return (
        outputStr + registry.findIndex((value) => normalize(signal) === value)
      );
    }, "");
    return sum + parseInt(lineOutput);
  }, 0);
};

run({
  part1: {
    tests: [
      {
        input: `
          be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
          edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
          fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
          fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
          aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
          fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
          dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
          bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
          egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
          gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
      `,
        expected: 26,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
          edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
          fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
          fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
          aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
          fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
          dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
          bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
          egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
          gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
      `,
        expected: 61229,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});

import run from "aocrunner";

type Coord = {
  x: number;
  y: number;
};

type Fold = {
  axis: string;
  value: number;
};

const parseInput = (rawInput: string): { coords: Coord[]; folds: Fold[] } => {
  const [coords, folds] = rawInput
    .split("\n\n")
    .map((part) => part.split("\n"));
  return {
    coords: coords.map((coord) => {
      const [x, y] = coord.split(",");
      return { x: Number(x), y: Number(y) };
    }),
    folds: folds.map((fold) => {
      const [axis, value] = fold.replace("fold along ", "").split("=");
      return { axis, value: Number(value) };
    }),
  };
};

const foldSheet = (coords: Coord[], fold: Fold) => {
  for (let i = coords.length - 1; i >= 0; i--) {
    coords[i][fold.axis] =
      coords[i][fold.axis] < fold.value
        ? coords[i][fold.axis]
        : fold.value - (coords[i][fold.axis] - fold.value);
  }
};

const draw = (coords: Coord[]) => {
  const yMax = Math.max(...coords.map(({ y }) => y));
  const xMax = Math.max(...coords.map(({ x }) => x));

  const sheet = Array.from({ length: yMax + 1 }, () =>
    Array.from({ length: xMax + 1 }, () => "."),
  );

  coords.forEach(({ x, y }) => {
    sheet[y][x] = "#";
  });

  return sheet.map((row) => row.join("")).join("\n");
};

const part1 = (rawInput: string) => {
  const { coords, folds } = parseInput(rawInput);
  foldSheet(coords, folds[0]);
  const set = new Set(coords.map(({ x, y }) => `${x},${y}`));
  return set.size;
};

const part2 = (rawInput: string) => {
  const { coords, folds } = parseInput(rawInput);
  folds.forEach((fold) => foldSheet(coords, fold));
  const drawing = draw(coords);
  console.log(drawing.replaceAll(".", " "));
  return drawing;
};

run({
  part1: {
    tests: [
      {
        input: `
          6,10
          0,14
          9,10
          0,3
          10,4
          4,11
          6,0
          6,12
          4,1
          0,13
          10,12
          3,4
          3,0
          8,4
          1,10
          2,14
          8,10
          9,0
          
          fold along y=7
          fold along x=5
        `,
        expected: 17,
      },
    ],
    solution: part1,
  },
  part2: {
    tests: [
      {
        input: `
          6,10
          0,14
          9,10
          0,3
          10,4
          4,11
          6,0
          6,12
          4,1
          0,13
          10,12
          3,4
          3,0
          8,4
          1,10
          2,14
          8,10
          9,0
          
          fold along y=7
          fold along x=5
        `,
        expected: `#####
#...#
#...#
#...#
#####`,
      },
    ],
    solution: part2,
  },
  trimTestInputs: true,
  onlyTests: false,
});
